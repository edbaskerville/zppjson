#ifndef __zppjson_Int64__
#define __zppjson_Int64__


#include "Optional.hpp"
#include <stdexcept>

namespace zppjson {

class Int64 : public Optional
{
public:
	Int64() :
		Optional(false)
	{
	}
	
	Int64(int64_t value) :
		Optional(true), _value(value)
	{
	}
	
	Int64(Int64 const & obj) :
		Optional(obj), _value(obj._value)
	{
	}
	
	void setValue(int64_t value)
	{
		_value = value;
		setPresent(true);
	}
	
	Int64 & operator=(int64_t value)
	{
		setValue(value);
		return *this;
	}
	
	Int64 & operator=(std::nullptr_t nullValue)
	{
		setPresent(false);
		return *this;
	}

	operator int64_t() const
	{
		if(!present()) {
			throw std::runtime_error("Cannot return int64_t value for non-present Int64");
		}
		return _value;
	}
	
	void loadJson(JsonElement const & jsonEl)
	{
		*this = jsonEl.int64Value();
	}
	
	void printJson(json_printer & printer)
	{
		if(present()) {
			std::stringstream ss;
			ss << _value;
			std::string str = ss.str();
			json_print_pretty(&printer, JSON_INT, str.c_str(), (uint32_t)str.length());
		}
		else {
			json_print_pretty(&printer, JSON_NULL, nullptr, 0);
		}
	}
private:
	int64_t _value;
};

} // namespace zppjson

#endif // #ifndef __zppjson_Int64__
