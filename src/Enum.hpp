#ifndef __zppjson_Enum__
#define __zppjson_Enum__


#include "Optional.hpp"
#include <stdexcept>
#include <vector>
#include <unordered_map>

namespace zppjson {

#define ZPPJSON_DEFINE_ENUM_VALUE_FUNCTION(r, data, i, valueName) \
static int64_t valueName() \
{ \
	return i; \
}

#define ZPPJSON_NAME_FROM_VALUE_ENTRY(r, value, i, valueName) \
if(value == i) { \
	return BOOST_PP_STRINGIZE(valueName); \
}

#define ZPPJSON_VALUE_FROM_NAME_ENTRY(r, name, i, valueName) \
if(name == BOOST_PP_STRINGIZE(valueName)) { \
	return i; \
}

#define ZPPJSON_DEFINE_ENUM(T, enumSeq) \
class T : public zppjson::Optional \
{ \
public: \
	T() : Optional(false) { } \
	\
	BOOST_PP_SEQ_FOR_EACH_I(ZPPJSON_DEFINE_ENUM_VALUE_FUNCTION, data, enumSeq) \
	\
	T(int64_t const & value) : \
		Optional(true), _value(value) \
	{ \
	} \
	\
	T(T const & obj) : \
		Optional(obj), _value(obj._value) \
	{ \
	} \
	\
	void setValue(int64_t value) \
	{ \
		_value = value; \
		setPresent(true); \
	} \
	\
	T & operator=(int64_t value) \
	{ \
		setValue(value); \
		return *this; \
	} \
	\
	T & operator=(std::nullptr_t nullValue) \
	{ \
		setPresent(false); \
		return *this; \
	} \
	\
	operator int64_t() \
	{ \
		if(!present()) { \
			throw std::runtime_error("Cannot return int64_t value for non-present enum"); \
		} \
		return _value; \
	} \
	\
	void loadJson(JsonElement const & jsonEl) \
	{ \
		std::string str = jsonEl.stringValue(); \
		setValue(valueFromName(str)); \
	} \
	\
	void printJson(json_printer & printer) \
	{ \
		if(present()) { \
			std::string str = nameFromValue(_value); \
			json_print_pretty(&printer, JSON_STRING, str.c_str(), (uint32_t)str.length()); \
		} \
		else { \
			json_print_pretty(&printer, JSON_NULL, nullptr, 0); \
		} \
	} \
private: \
	int64_t _value; \
	\
	std::string nameFromValue(int64_t value) \
	{ \
		BOOST_PP_SEQ_FOR_EACH_I(ZPPJSON_NAME_FROM_VALUE_ENTRY, value, enumSeq) \
		throw std::runtime_error("Invalid enum value"); \
	} \
	\
	int64_t valueFromName(std::string const & name) \
	{ \
		BOOST_PP_SEQ_FOR_EACH_I(ZPPJSON_VALUE_FROM_NAME_ENTRY, name, enumSeq) \
		throw std::runtime_error("Invalid enum name " + name); \
	} \
};

} // namespace zppjson

#endif // #ifndef __zppjson_Enum__
