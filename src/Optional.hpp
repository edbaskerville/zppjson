#ifndef __zppjson_Optional__
#define __zppjson_Optional__

#include <iostream>
#include "json.h"
#include <stdint.h>
#include <sstream>

namespace zppjson {

class Optional
{
public:
	Optional(Optional const & obj) :
		_present(obj._present)
	{
	}
	
	Optional() :
		_present(false)
	{
	}
	
	Optional(bool present) :
		_present(present)
	{
	}
	
	bool present() const
	{
		return _present;
	}
	
	static int
	printerCallback(void * userdata, char const * s, uint32_t length)
	{
		std::ostream * stream  = (std::ostream *)userdata;
		*stream << std::string(s, length);
		return 0;
	}
	
	virtual void printJsonToStream(std::ostream & stream)
	{
		json_printer printer;
		json_print_init(&printer, printerCallback, (void *)&stream);
		printJson(printer);
		json_print_free(&printer);
		stream << std::endl;
	}
	
	virtual void printJson(json_printer & printer) = 0;
	
	std::string toJsonString()
	{
		std::stringstream ss;
		printJsonToStream(ss);
		return ss.str();
	}
protected:
	void setPresent(bool present)
	{
		_present = present;
	}
private:
	bool _present;
};

} // namespace zppjson

#endif // #ifndef __zppjson_Optional__
