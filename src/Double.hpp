#ifndef __zppjson_Double__
#define __zppjson_Double__

#include "Optional.hpp"
#include "json.h"
#include <sstream>
#include <stdexcept>

namespace zppjson {

class Double : public Optional
{
public:
	Double() :
		Optional(false)
	{
	}
	
	Double(double value) :
		Optional(true), _value(value)
	{
	}
	
	Double(Double const & obj) :
		Optional(obj), _value(obj._value)
	{
	}
	
	void setValue(double value)
	{
		_value = value;
		setPresent(true);
	}
	
	Double & operator=(double value)
	{
		setValue(value);
		return *this;
	}
	
	Double & operator=(std::nullptr_t nullValue)
	{
		setPresent(false);
		return *this;
	}

	operator double() const
	{
		if(!present()) {
			throw std::runtime_error("Cannot return double value for non-present Double");
		}
		return _value;
	}
	
	void loadJson(JsonElement const & jsonEl)
	{
		*this = jsonEl.doubleValue();
	}
	
	void printJson(json_printer & printer)
	{
		if(present()) {
			std::stringstream ss;
			ss << _value;
			std::string str = ss.str();
			json_print_pretty(&printer, JSON_FLOAT, str.c_str(), (uint32_t)str.length());
		}
		else {
			json_print_pretty(&printer, JSON_NULL, nullptr, 0);
		}
	}
private:
	double _value;
};

} // namespace zppjson

#endif // #ifndef __zppjson_Double__
