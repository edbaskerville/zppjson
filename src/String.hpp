#ifndef __zppjson_String__
#define __zppjson_String__

#include "Optional.hpp"
#include <stdexcept>

namespace zppjson {

class String : public Optional
{
public:
	String() :
		Optional(false)
	{
	}
	
	String(std::string const & value) :
		Optional(true), _value(value)
	{
	}
	
	String(String const & obj) :
		Optional(obj), _value(obj._value)
	{
	}
	
	void setValue(std::string const & value)
	{
		_value = value;
		setPresent(true);
	}
	
	String & operator=(std::string const & value)
	{
		setValue(value);
		return *this;
	}
	
	String & operator=(std::nullptr_t nullValue)
	{
		setPresent(false);
		return *this;
	}

	std::string toString()
	{
		if(!present()) {
			throw std::runtime_error("Cannot return std::string value for non-present String");
		}
		return _value;
	}

	operator std::string() const
	{
		if(!present()) {
			throw std::runtime_error("Cannot return double value for non-present Double");
		}
		return _value;
	}
	
	void loadJson(JsonElement const & jsonEl)
	{
		*this = jsonEl.stringValue();
	}
	
	void printJson(json_printer & printer)
	{
		if(present()) {
			json_print_pretty(&printer, JSON_STRING, _value.c_str(), (uint32_t)_value.length());
		}
		else {
			json_print_pretty(&printer, JSON_NULL, nullptr, 0);
		}
	}
private:
	std::string _value;
};

} // namespace zppjson

#endif // #ifndef __zppjson_String__
