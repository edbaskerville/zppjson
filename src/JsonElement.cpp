#include "JsonElement.hpp"
#include <sstream>
#include <stdexcept>

using namespace std;

namespace zppjson {

JsonException::JsonException(std::string const & errStr) :
	std::runtime_error(errStr)
{
}

JsonElement::JsonElement(JsonElement const & obj) :
	_type(obj._type), _str(obj._str), _keys(nullptr), _children(nullptr), _keyIndexMap(nullptr)
{
	if(obj._keys != nullptr) {
		_keys = new vector<string>(obj._keys->begin(), obj._keys->end());
		_keyIndexMap = new unordered_map<string, int64_t>(obj._keyIndexMap->begin(), obj._keyIndexMap->end());
	}
	
	if(obj._children != nullptr) {
		_children = new std::vector<JsonElement *>();
		_children->reserve(obj._children->size());
		for(JsonElement * child : *(obj._children)) {
			_children->push_back(new JsonElement(*child));
		}
	}
}

JsonElement::JsonElement(JsonType type, std::string const & str) :
	_type(type), _str(str), _keys(nullptr), _children(nullptr), _keyIndexMap(nullptr)
{
	if(type == JsonType::OBJECT) {
		_keys = new std::vector<std::string>();
		_keyIndexMap = new std::unordered_map<std::string, int64_t>();
	}
	if(type == JsonType::OBJECT || type == JsonType::ARRAY) {
		_children = new std::vector<JsonElement *>();
	}
}
	
JsonElement::~JsonElement()
{
	if(_keys != nullptr) {
		delete _keys;
		delete _keyIndexMap;
	}
	
	if(_children != nullptr) {
		for(JsonElement * child : *_children) {
			delete child;
		}
		delete _children;
	}
}

JsonType
JsonElement::type() const
{
	return _type;
}

bool
JsonElement::boolValue() const
{
	switch(_type) {
		case JsonType::BOOL_FALSE:
			return false;
		case JsonType::BOOL_TRUE:
			return true;
		default:
			throw JsonException("Only TRUE or FALSE can return boolValue()");
	}
}

int64_t
JsonElement::int64Value() const
{
	switch(_type) {
		case JsonType::INTEGER:
		case JsonType::STRING:
			return stoll(_str);
		default:
			throw JsonException("Only INTEGER can return integerValue()");
	}
}

double
JsonElement::doubleValue() const
{
	switch(_type) {
		case JsonType::INTEGER:
		case JsonType::REAL:
		case JsonType::STRING:
			return stod(_str);
		default:
			throw JsonException("Only REAL can return doubleValue()");
	}
}

std::string
JsonElement::stringValue() const
{
	switch(_type) {
		case JsonType::NULL_OBJECT:
			return "null";
		case JsonType::BOOL_TRUE:
			return "true";
		case JsonType::BOOL_FALSE:
			return "false";
		case JsonType::INTEGER:
		case JsonType::REAL:
		case JsonType::STRING:
			return _str;
		default:
			throw JsonException("Only primitive types can return stringValue()");
			
	}
}

void
JsonElement::addKey(std::string const & key)
{
	if(_type != JsonType::OBJECT) {
		throw JsonException("Only objects can have keys");
	}
	
	if(_keys->size() != _children->size()) {
		throw JsonException("Cannot add key unless keys and children are currently matched.");
	}
	
	_keys->push_back(key);
}

JsonElement *
JsonElement::addChild(JsonType childType, std::string const & childStr)
{
	switch(_type) {
		case JsonType::ARRAY:
			break;
		case JsonType::OBJECT:
			if(_keys->size() != _children->size() + 1) {
				throw JsonException("Cannot add child for key unless there is exactly one extra key.");
			}
			(*_keyIndexMap)[_keys->back()] = _keys->size() - 1;
			break;
		default:
			throw JsonException("Only arrays and objects can have children");
	}
	
	_children->push_back(new JsonElement(childType, childStr));
	return _children->back();
}

bool
JsonElement::containsKey(std::string const & key) const
{
	if(_type != JsonType::OBJECT) {
		throw runtime_error("Can only check for key in objects");
	}
	
	return _keyIndexMap->find(key) != _keyIndexMap->end();
}

std::vector<std::string> const &
JsonElement::keys() const
{
	if(_type != JsonType::OBJECT) {
		throw runtime_error("Can only get keys from objects");
	}
	
	return *_keys;
}

JsonElement const &
JsonElement::operator[](std::string const & key) const
{
	if(!containsKey(key)) {
		throw runtime_error("Does not contain key " + key);
	}
	
	return *((*_children)[(*_keyIndexMap)[key]]);
}

void
JsonElement::print(json_printer & printer)
{
	switch(_type) {
		case JsonType::NULL_OBJECT:
			json_print_pretty(&printer, JSON_NULL, nullptr, 0);
			break;
		case JsonType::BOOL_TRUE:
			json_print_pretty(&printer, JSON_TRUE, nullptr, 0);
			break;
		case JsonType::BOOL_FALSE:
			json_print_pretty(&printer, JSON_FALSE, nullptr, 0);
			break;
		case JsonType::INTEGER:
			{
				stringstream strStream;
				strStream << int64Value();
				string str = strStream.str();
				json_print_pretty(&printer, JSON_INT, str.c_str(), (uint32_t)str.length());
			}
			break;
		case JsonType::REAL:
			{
				stringstream strStream;
				strStream << doubleValue();
				string str = strStream.str();
				json_print_pretty(&printer, JSON_FLOAT, str.c_str(), (uint32_t)str.length());
			}
			break;
		case JsonType::STRING:
			{
				string str = stringValue();
				json_print_pretty(&printer, JSON_STRING, str.c_str(), (uint32_t)str.length());
			}
			break;
		case JsonType::ARRAY:
			json_print_pretty(&printer, JSON_ARRAY_BEGIN, nullptr, 0);
			for(JsonElement * child : *_children) {
				child->print(printer);
			}
			json_print_pretty(&printer, JSON_ARRAY_END, nullptr, 0);
			break;
		case JsonType::OBJECT:
			json_print_pretty(&printer, JSON_OBJECT_BEGIN, nullptr, 0);
			for(auto & keyIndex : *_keyIndexMap) {
				string key = keyIndex.first;
				json_print_pretty(&printer, JSON_KEY, key.c_str(), (uint32_t)key.length());
				(*_children)[keyIndex.second]->print(printer);
			}
			json_print_pretty(&printer, JSON_OBJECT_END, nullptr, 0);
			break;
	}
}

size_t
JsonElement::size() const
{
	if(!(_type == JsonType::OBJECT || _type == JsonType::ARRAY)) {
		throw runtime_error("Only objects or arrays can have size queried");
	}
	
	return _children->size();
}

JsonElement const &
JsonElement::operator[](size_t index) const
{
	if(index >= _children->size()) {
		throw runtime_error("Index out of bounds");
	}
	
	return *((*_children)[index]);
}

} // namespace zppjson
