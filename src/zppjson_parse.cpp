#include "zppjson_parse.hpp"
#include "json.h"
#include "JsonElement.hpp"
#include <cassert>

using namespace std;

namespace zppjson {

struct ParserContext
{
	JsonElement * root = nullptr;
	std::vector<JsonElement *> stack;
};

static int parserCallback(void * userData, int type, const char *data, uint32_t length)
{
	ParserContext * context = (ParserContext *)userData;
	std::vector<JsonElement *> * stack = &context->stack;
	
	JsonElement * top = nullptr;
	if(stack->size() > 0) {
		top = stack->back();
	}
	
	string dataStr(data, length);
	
	bool shouldMakeObject = false;
	JsonType newType = JsonType::NULL_OBJECT;
	switch(type) {
		case JSON_ARRAY_BEGIN:
//			cerr << "array begin" << endl;
			shouldMakeObject = true;
			newType = JsonType::ARRAY;
			break;
		case JSON_OBJECT_BEGIN:
//			cerr << "object begin" << endl;
			shouldMakeObject = true;
			newType = JsonType::OBJECT;
			break;
		case JSON_ARRAY_END:
//			cerr << "array end" << endl;
			assert(top != nullptr);
			assert(context->root != nullptr);
			stack->pop_back();
			break;
		case JSON_OBJECT_END:
//			cerr << "object end" << endl;
			assert(top != nullptr);
			assert(context->root != nullptr);
			stack->pop_back();
			break;
		case JSON_INT:
//			cerr << "int: " << std::string(data, length) << endl;
			shouldMakeObject = true;
			newType = JsonType::INTEGER;
			break;
		case JSON_FLOAT:
//			cerr << "float: " << std::string(data, length) << endl;
			shouldMakeObject = true;
			newType = JsonType::REAL;
			break;
		case JSON_STRING:
//			cerr << "string: " << std::string(data, length) << endl;
			shouldMakeObject = true;
			newType = JsonType::STRING;
			break;
		case JSON_KEY:
//			cerr << "key: " << std::string(data, length) << endl;
			assert(top->type() == JsonType::OBJECT);
			top->addKey(dataStr);
			break;
		case JSON_TRUE:
//			cerr << "true: " << std::string(data, length) << endl;
			shouldMakeObject = true;
			newType = JsonType::BOOL_TRUE;
			break;
		case JSON_FALSE:
//			cerr << "false: " << std::string(data, length) << endl;
			shouldMakeObject = true;
			newType = JsonType::BOOL_FALSE;
			break;
		case JSON_NULL:
//			cerr << "null: " << std::string(data, length) << endl;
			shouldMakeObject = true;
			newType = JsonType::NULL_OBJECT;
			break;
		case JSON_BSTRING:
//			cerr << "bstring: " << std::string(data, length) << endl;
			assert(false);
			break;
	}
	
	if(shouldMakeObject) {
		JsonElement * newObj;
		if(top == nullptr) {
			assert(context->root == nullptr);
			newObj = new JsonElement(newType, dataStr);
			context->root = newObj;
		}
		else {
			assert(top != nullptr);
			assert(context->root != nullptr);
			assert(top->type() == JsonType::OBJECT || top->type() == JsonType::ARRAY);
			newObj = top->addChild(newType, dataStr);
		}
		if(newType == JsonType::OBJECT || newType == JsonType::ARRAY) {
			stack->push_back(newObj);
		}
	}
	
	return 0;
}

static std::string uncommentLine(std::string const & line)
{
	for(size_t i = 0; i < line.size() - 1; i++) {
		if(line.substr(i, 2) == "//") {
			return line.substr(0, i);
		}
	}
	return line;
}

JsonElement parseJson(std::istream & stream)
{
	ParserContext context;
	
	json_parser parser;
	json_parser_init(&parser, nullptr, parserCallback, &context);
	
	std::string rawLine;
	while(std::getline(stream, rawLine)) {
		std::string line = uncommentLine(rawLine);
		for(size_t i = 0; i < line.size(); i++) {
			json_parser_char(&parser, line[i]);
		}
	}
	json_parser_free(&parser);
	
	return *(context.root);
}

static int
PrinterCallback(void * userdata, char const * s, uint32_t length)
{
	std::ostream * stream  = (std::ostream *)userdata;
	*stream << std::string(s, length);
	return 0;
}

void
printJson(JsonElement & jsonObj, std::ostream & stream)
{
	json_printer printer;
	json_print_init(&printer, PrinterCallback, (void *)&stream);
	jsonObj.print(printer);
	json_print_free(&printer);
	stream << endl;
}

} // namespace zppjson
