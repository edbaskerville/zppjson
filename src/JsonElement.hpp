#ifndef __zppjson_JsonElement__
#define __zppjson_JsonElement__

#include <string>
#include <memory>
#include <vector>
#include <unordered_map>
#include <iostream>
#include <stdexcept>
#include "json.h"

namespace zppjson {

enum class JsonType
{
	NULL_OBJECT,
	BOOL_TRUE,
	BOOL_FALSE,
	INTEGER,
	REAL,
	STRING,
	ARRAY,
	OBJECT
};

class JsonException : std::runtime_error
{
public:
	JsonException(std::string const & error);
};

class JsonElement
{
public:
	JsonElement(JsonElement const & obj);
//	JsonElement(JsonType type);
	JsonElement(JsonType type, std::string const & str);
	
	~JsonElement();
	
	JsonType type() const;
	
	bool boolValue() const;
	int64_t int64Value() const;
	double doubleValue() const;
	std::string stringValue() const;
	
	void addKey(std::string const & key);
	JsonElement * addChild(JsonType childType, std::string const & childStr);
	
	bool containsKey(std::string const & key) const;
	std::vector<std::string> const & keys() const;
	JsonElement const & operator[](std::string const & key) const;
	
	size_t size() const;
	JsonElement const & operator[](size_t index) const;
	
	void print(json_printer & printer);
	
private:
	JsonType _type;
	std::string _str;
	std::vector<std::string> * _keys;
	std::vector<JsonElement *> * _children;
	
	std::unordered_map<std::string, int64_t> * _keyIndexMap;
};

} // namespace zppjson

#endif // #ifndef __zppjson_JsonElement__
