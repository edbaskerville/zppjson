#ifndef __zppjson_Bool__
#define __zppjson_Bool__


#include "Optional.hpp"
#include <stdexcept>

namespace zppjson {

class Bool : public Optional
{
public:
	Bool() :
		Optional(false)
	{
	}
	
	Bool(bool value) :
		Optional(true), _value(value)
	{
	}
	
	Bool(Bool const & obj) :
		Optional(obj), _value(obj._value)
	{
	}
	
	void setValue(bool value)
	{
		_value = value;
		setPresent(true);
	}
	
	Bool & operator=(bool value)
	{
		setValue(value);
		return *this;
	}
	
	Bool & operator=(std::nullptr_t nullValue)
	{
		setPresent(false);
		return *this;
	}

	operator bool()
	{
		if(!present()) {
			throw std::runtime_error("Cannot return bool value for non-present Bool");
		}
		return _value;
	}
	
	void loadJson(JsonElement const & jsonEl)
	{
		*this = jsonEl.boolValue();
	}
	
	void printJson(json_printer & printer)
	{
		if(present()) {
			json_print_pretty(&printer, _value ? JSON_TRUE : JSON_FALSE, nullptr, 0);
		}
		else {
			json_print_pretty(&printer, JSON_NULL, nullptr, 0);
		}
	}
private:
	bool _value;
};

} // namespace zppjson

#endif // #ifndef __zppjson_Bool__
