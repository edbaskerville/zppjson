#ifndef __zppjson_Map__
#define __zppjson_Map__

#include "Optional.hpp"
#include <unordered_map>

namespace zppjson {

template<typename V>
class Map : public Optional
{
public:
	
	Map() :
		Optional(false)
	{
	}
	
	Map(Map const & obj) :
		Optional(obj), _keys(obj._keys), _values(obj._values), _keyIndexMap(obj._keyIndexMap)
	{
	}
	
	void loadJson(JsonElement const & jsonEl)
	{
		for(auto & key : jsonEl.keys()) {
			if(_keyIndexMap.find(key) == _keyIndexMap.end()) {
				_keyIndexMap[key] = _keys.size();
				_keys.push_back(key);
				_values.emplace_back();
			}
			_values[_keyIndexMap[key]].loadJson(jsonEl[key]);
		}
		setPresent(true);
	}
	
	void printJson(json_printer & printer)
	{
		if(present()) {
			json_print_pretty(&printer, JSON_OBJECT_BEGIN, nullptr, 0);
			for(auto keyIndex : _keyIndexMap) {
				int64_t index = keyIndex.second;
				if(_values[index].present()) {
					std::string key = keyIndex.first;
					json_print_pretty(&printer, JSON_KEY, key.c_str(), (uint32_t)key.length());
					_values[index].printJson(printer);
				}
			}
			json_print_pretty(&printer, JSON_OBJECT_END, nullptr, 0);
		}
		else {
			json_print_pretty(&printer, JSON_NULL, nullptr, 0);
		}
	}
	
	std::vector<std::string> keys()
	{
		if(!present()) {
			throw JsonException("Map not present");
		}
		return _keys;
	}
	
	bool containsKey(std::string const & key)
	{
		if(!present()) {
			throw JsonException("Map not present");
		}
		return _keyIndexMap.find(key) != _keyIndexMap.end();
	}
	
	V & operator[](std::string const & key)
	{
		if(!present()) {
			throw JsonException("Map not present");
		}
		return _values[_keyIndexMap[key]];
	}
private:
	std::vector<std::string> _keys;
	std::vector<V> _values;
	std::unordered_map<std::string, int64_t> _keyIndexMap;
};

} // namespace zppjson

#endif // _#ifndef __zppjson_Map__
