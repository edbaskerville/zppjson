#ifndef __zppjson_parse__
#define __zppjson_parse__

#include <iostream>
#include "JsonElement.hpp"

namespace zppjson {

JsonElement parseJson(std::istream & stream);
void printJson(JsonElement & jsonObj, std::ostream & stream);

} // namespace zppjson

#endif
