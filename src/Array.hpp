#ifndef __zppjson_Array__
#define __zppjson_Array__

#include "Optional.hpp"
#include <stdexcept>

namespace zppjson {

template<typename V>
class Array : public Optional
{
public:
	
	Array() :
		Optional(false)
	{
	}
	
	Array(Array const & obj) :
		Optional(obj), _values(obj._values)
	{
	}
	
	void loadJson(JsonElement const & jsonEl)
	{
		if(jsonEl.type() != JsonType::ARRAY) {
			throw std::runtime_error("Only JSON arrays can be loaded into Array objects");
		}
		size_t size = jsonEl.size();
		
		// If already present, check that sizes match
		if(present()) {
			if(_values.size() != size) {
				throw std::runtime_error("If loading into existing array, sizes must match");
			}
		}
		// Otherwise, create new elements
		else {
			_values.clear();
			_values.reserve(size);
			for(size_t i = 0; i < size; i++) {
				_values.emplace_back();
			}
			setPresent(true);
		}
		
		// Load values for elements
		for(size_t i = 0; i < size; i++) {
			_values[i].loadJson(jsonEl[i]);
		}
	}
	
	void printJson(json_printer & printer)
	{
		if(present()) {
			json_print_pretty(&printer, JSON_ARRAY_BEGIN, nullptr, 0);
			for(auto & value : _values) {
				value.printJson(printer);
			}
			json_print_pretty(&printer, JSON_ARRAY_END, nullptr, 0);
		}
		else {
			json_print_pretty(&printer, JSON_NULL, nullptr, 0);
		}
	}
	
	V & operator[](size_t const & index)
	{
		if(!present()) {
			throw JsonException("Array not present");
		}
		return _values[index];
	}
	
	std::vector<std::string> toStringVector()
	{
		std::vector<std::string> strVec;
		strVec.reserve(_values.size());
		for(V & val : _values) {
			strVec.push_back(val.toString());
		}
		return strVec;
	}
	
	std::vector<int64_t> toInt64Vector()
	{
		std::vector<int64_t> vec;
		vec.reserve(_values.size());
		for(V & val : _values) {
			vec.push_back(val);
		}
		return vec;
	}
	
	std::vector<double> toDoubleVector()
	{
		std::vector<double> vec;
		vec.reserve(_values.size());
		for(V & val : _values) {
			vec.push_back(val);
		}
		return vec;
	}
	
	void push_back(V const & element)
	{
		setPresent(true);
		_values.push_back(element);
	}
	
	template<typename ... Args>
	void emplace_back(Args && ... args)
	{
		setPresent(true);
		_values.emplace_back(args...);
	}
	
	size_t size() const
	{
		return _values.size();
	}
	
	std::vector<V> & values()
	{
		return _values;
	}
private:
	std::vector<V> _values;
};

} // namespace zppjson

#endif // _#ifndef __zppjson_Map__
