#ifndef __zppjson_macros__
#define __zppjson_macros__

#include "Optional.hpp"
#include <boost/preprocessor.hpp>
#include "string.h"

#define ZPPJSON_DEFINE_FIELD(r, data, typeSpec) \
BOOST_PP_SEQ_ELEM(0, typeSpec) BOOST_PP_SEQ_ELEM(1, typeSpec);

#define ZPPJSON_LOAD_FIELD(r, data, typeSpec) \
if(jsonObj.containsKey(BOOST_PP_STRINGIZE(BOOST_PP_SEQ_ELEM(1, typeSpec)))) \
{ \
	BOOST_PP_SEQ_ELEM(1, typeSpec).loadJson(jsonObj[BOOST_PP_STRINGIZE(BOOST_PP_SEQ_ELEM(1, typeSpec))]); \
}

#define ZPPJSON_PRINT_FIELD(r, data, typeSpec) \
if(BOOST_PP_SEQ_ELEM(1, typeSpec).present()) { \
	json_print_pretty( \
		&printer, \
		JSON_KEY, \
		BOOST_PP_STRINGIZE(BOOST_PP_SEQ_ELEM(1, typeSpec)), \
		strlen(BOOST_PP_STRINGIZE(BOOST_PP_SEQ_ELEM(1, typeSpec))) \
	); \
	BOOST_PP_SEQ_ELEM(1, typeSpec).printJson(printer); \
}

#define ZPPJSON_DEFINE_TYPE(typeName, typeSeq) \
class typeName : public Optional \
{ \
public: \
	typeName() : Optional(false) { } \
	\
	BOOST_PP_SEQ_FOR_EACH(ZPPJSON_DEFINE_FIELD, data, typeSeq) \
	\
	void loadJson(JsonElement const & jsonObj) \
	{ \
		setPresent(true); \
		BOOST_PP_SEQ_FOR_EACH(ZPPJSON_LOAD_FIELD, data, typeSeq) \
	} \
	\
	virtual void printJson(json_printer & printer) \
	{ \
		json_print_pretty(&printer, JSON_OBJECT_BEGIN, nullptr, 0); \
		BOOST_PP_SEQ_FOR_EACH(ZPPJSON_PRINT_FIELD, data, typeSeq) \
		json_print_pretty(&printer, JSON_OBJECT_END, nullptr, 0); \
	} \
};


#endif // #ifndef __zppjson_macros__
