#!/usr/bin/env python

import os
import sys
import re
import cStringIO

def processFile(filename, stream):
	with open(filename, 'rU') as file:
		fileStrIO = cStringIO.StringIO()
		for line in file:
			fileStrIO.write(line)
		fileStr = fileStrIO.getvalue()
		fileStrIO.close()
		
		ws = r'\s*'
		doxygenCommentBlock = r'/\*\*' + r'(?:(?!\*/).)*?' + r'\*/'
		
		subPatternTokens = [
			r'(',
			doxygenCommentBlock,
			r')',
			ws,
			r'\(',
			ws,
			r'\(',
			ws,
			r'([^\(\)]+)',
			ws,
			r'\)',
			ws,
			r'\(',
			ws,
			r'([^\(\)]+)',
			ws,
			r'\)',
			ws,
			r'\)'
		]
		
		patternTokens = [
			r'(',
			doxygenCommentBlock,
			r')',
			ws,
			r'ZPPJSON_DEFINE_TYPE',
			ws,
			r'\(',
			ws,
			r'(\w+)',
			ws,
			r',((',
			ws
		] + subPatternTokens + [
			r')*)',
			ws,
			r'\)'
		]
		
		subPatternStr = ''.join(subPatternTokens)
		patternStr = ''.join(patternTokens)
		
		pattern = re.compile(patternStr, flags=re.DOTALL)
		subPattern = re.compile(subPatternStr, flags=re.DOTALL)
		
		for match in re.finditer(pattern, fileStr):
			fullMatch = match.group(0)
			
			initialComment = match.group(1)
			className = match.group(2)
			fieldSection = match.group(3)
			
			stream.write(initialComment)
			stream.write('\n')
			stream.write('class {0}\n{{\npublic:\n'.format(className))
			
			fieldSectionNew = re.sub(subPattern, r'\1\n\t\2 \3;', fieldSection)
			stream.write(fieldSectionNew)
			
			stream.write('\n};\n')
				

if __name__ == '__main__':
	for filename in sys.argv[1:]:
		processFile(filename, sys.stdout)
