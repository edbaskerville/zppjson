#include "json.h"
#include "catch.hpp"
#include <iostream>

using namespace std;

int parserCallback(void * userData, int type, const char *data, uint32_t length)
{
	string typeStr;
	switch(type) {
		case JSON_ARRAY_BEGIN:
			typeStr = "JSON_ARRAY_BEGIN";
			break;
		case JSON_OBJECT_BEGIN:
			typeStr = "JSON_OBJECT_BEGIN";
			break;
		case JSON_ARRAY_END:
			typeStr = "JSON_ARRAY_END";
			break;
		case JSON_OBJECT_END:
			typeStr = "JSON_OBJECT_END";
			break;
		case JSON_INT:
			typeStr = "JSON_INT";
			break;
		case JSON_FLOAT:
			typeStr = "JSON_FLOAT";
			break;
		case JSON_STRING:
			typeStr = "JSON_STRING";
			break;
		case JSON_KEY:
			typeStr = "JSON_KEY";
			break;
		case JSON_TRUE:
			typeStr = "JSON_TRUE";
			break;
		case JSON_FALSE:
			typeStr = "JSON_FALSE";
			break;
		case JSON_NULL:
			typeStr = "JSON_NULL";
			break;
		case JSON_BSTRING:
			typeStr = "JSON_BSTRING";
			break;
	}
	
	cerr << "parser callback: " << typeStr << ", " << string(data, length) << ", " << length << endl;
	return 0;
}

TEST_CASE("libjson test runs", "[libjson]")
{
	json_parser parser;
	json_parser_init(&parser, nullptr, parserCallback, nullptr);
	
	string srcJsonStr = R"({
		"a" : 0.5,
		"b" : "hello there mr",
		"c" : 100,
		"d" : {
			"obj1" : {
				"a" : 0.1,
				"b" : null,
				"c" : {
					"x" : 0.1,
					"y" : 0.2,
					"z" : 0.3
				}
			},
			"obj2" : {
				"a" : 0.4,
				"b" : 0.5,
				"c" : {
					"x" : 0.4,
					"y" : 0.5,
					"z" : 0.6
				}
			
			}
		}
	})";

	for(int64_t i = 0; i < srcJsonStr.length(); i++) {
		json_parser_char(&parser, srcJsonStr[i]);
	}
}
