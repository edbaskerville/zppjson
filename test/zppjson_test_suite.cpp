#include "zppjson.hpp"
#include "catch.hpp"
#include <iostream>
#include <sstream>

using namespace std;
using namespace zppjson;

static string srcJsonStr = R"({
	"a" : 0.5,
	"b" : "hello there mr",
	"c" : 100,
	"d" : {
		"obj1" : {
			"a" : [0.1, 0.2, 0.3],
			"b" : null,
			"c" : {
				"x" : 0.1,
				"y" : 0.2,
				"z" : 0.3
			}
		},
		"obj2" : {
			"a" : 0.4,
			"b" : 0.5,
			"c" : {
				"x" : 0.4,
				"y" : 0.5,
				"z" : 0.6
			}
		
		}
	}
})";

TEST_CASE("JsonElement basic test", "[JsonElement]")
{
	JsonElement jsonElem(JsonType::NULL_OBJECT, "null");
}

TEST_CASE("parseJson basic test", "[parseJson]")
{
	stringstream ss(srcJsonStr);
	JsonElement parsedJson = parseJson(ss);
}

TEST_CASE("types basic tests", "[Double]")
{
	Double doubleObj;
	REQUIRE(!doubleObj.present());
	
	doubleObj = 5.0;
	REQUIRE(doubleObj.present());
	REQUIRE(doubleObj == 5.0);
	
	doubleObj = nullptr;
	REQUIRE(!doubleObj.present());
}

class Member
{
public:
	Double r;
	Int64 s;
	String t;
	
	void loadJson(JsonElement const & jsonObj)
	{
		if(jsonObj.containsKey("r")) {
			r.loadJson(jsonObj["r"]);
		}
		if(jsonObj.containsKey("s")) {
			s.loadJson(jsonObj["s"]);
		}
		if(jsonObj.containsKey("t")) {
			t.loadJson(jsonObj["t"]);
		}
	}
};

class MyJson
{
public:
	Double x;
	Int64 y;
	String z;
	
	Map<Double> m1;
	Map<Member> m2;
	
	Array<Int64> a1;
	Array<Member> a2;
	
	void loadJson(JsonElement const & jsonObj)
	{
		if(jsonObj.containsKey("x")) {
			x.loadJson(jsonObj["x"]);
		}
		if(jsonObj.containsKey("y")) {
			y.loadJson(jsonObj["y"]);
		}
		if(jsonObj.containsKey("z")) {
			z.loadJson(jsonObj["z"]);
		}
		if(jsonObj.containsKey("m1")) {
			m1.loadJson(jsonObj["m1"]);
		}
		if(jsonObj.containsKey("m2")) {
			m2.loadJson(jsonObj["m2"]);
		}
		if(jsonObj.containsKey("a1")) {
			a1.loadJson(jsonObj["a1"]);
		}
		if(jsonObj.containsKey("a2")) {
			a2.loadJson(jsonObj["a2"]);
		}
	}
};


ZPPJSON_DEFINE_TYPE(MyAutoMember,
	( (Double)(r) )
	( (Int64)(s) )
	( (String)(t) )
	( (Bool)(u) )
)

ZPPJSON_DEFINE_TYPE(MyAutoJson,
	( (Double)(x) )
	( (Int64)(y) )
	( (String)(z) )
	( (Map<Double>)(m1) )
	( (Map<MyAutoMember>)(m2) )
	( (Array<Int64>)(a1) )
	( (Array<MyAutoMember>)(a2) )
	( (Array<String>)(a3) )
)

TEST_CASE("convert basic tests", "")
{
	string myJsonStr = R"({
		"x" : 0.5,
		"y" : 100,
		"z" : "hello",
		"m1" : {
			"a" : 2,
			"b" : 3.5,
			"c" : 100
		},
		"m2" : {
			"member1" : {
				"r" : 0.5,
				"s" : 100,
				"t" : "hellothere",
				"u" : false
			},
			// C++ Comment
			"member2" : {
				"r" : 0.3,
				"s" : 50,
				"t" : "gbye",
				"u" : true
			}
		},
		"a1" : [
			5,
			6,
			7
		],
		"a2" : [
		
				// C++ Test
			{
				// C++ Test
				"r" : 3.4,
				"s": 123,
				"t" : "blah",
				"u" : true
			},
			{
				"r" : 1.34,
				"s" : 234,
				"t" : "goog"
			}
		],
		"a3" : [
			"hello",
			"there"
		]
	})";
	
	stringstream ss(myJsonStr);
	JsonElement parsedJson = parseJson(ss);
	printJson(parsedJson, cerr);

	MyAutoJson myJson;
	myJson.loadJson(parsedJson);
	myJson.printJsonToStream(cerr);

	cerr << myJson.a2[1].r << endl;
	REQUIRE(myJson.m2.containsKey("member1"));
	REQUIRE(!myJson.m2.containsKey("member3"));

	vector<string> strVec = myJson.a3.toStringVector();
	for(auto & str : strVec) {
		cerr << str << endl;
	}
}

ZPPJSON_DEFINE_ENUM(
	MyEnumType,
	(BLUE)
	(RED)
	(YELLOW)
	(GREEN)
)

ZPPJSON_DEFINE_TYPE(
	MyEnumContainingType,
	((MyEnumType)(color1))
	((MyEnumType)(color2))
	((MyEnumType)(color3))
	((MyEnumType)(color4))
	((MyEnumType)(color5))
)

TEST_CASE("enum test", "[Enum]")
{
	string myJsonStr = R"({
		"color1" : "RED",
		"color2" : "BLUE",
		"color3" : "GREEN",
		"color4" : "YELLOW",
	})";
	
	stringstream ss(myJsonStr);
	JsonElement parsedJson = parseJson(ss);
	printJson(parsedJson, cerr);

	MyEnumContainingType obj;
	obj.loadJson(parsedJson);
	obj.printJsonToStream(cerr);

	cerr << obj.color1 << endl;
	cerr << obj.color2 << endl;
	cerr << obj.color3 << endl;
	cerr << obj.color4 << endl;

	int64_t red = MyEnumType::RED();
	int64_t blue = MyEnumType::BLUE();
	int64_t green = MyEnumType::GREEN();
	int64_t yellow = MyEnumType::YELLOW();

	REQUIRE(obj.color1.present());
	REQUIRE(obj.color1 == red);
	REQUIRE(obj.color2.present());
	REQUIRE(obj.color2 == blue);
	REQUIRE(obj.color3.present());
	REQUIRE(obj.color3 == green);
	REQUIRE(obj.color4.present());
	REQUIRE(obj.color4 == yellow);
	REQUIRE(!obj.color5.present());
}

TEST_CASE("append to array test", "[Array]")
{
	Array<Double> emptyArray;
	REQUIRE(!emptyArray.present());
	emptyArray.push_back(Double(5.5));
	REQUIRE(emptyArray.present());
	REQUIRE(emptyArray.size() == 1);
	REQUIRE(emptyArray[0] == 5.5);
	emptyArray.emplace_back(10.9);
	REQUIRE(emptyArray.size() == 2);
	REQUIRE(emptyArray[1] == 10.9);
}
